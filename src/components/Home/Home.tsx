import React from 'react'
import { Button } from '@progress/kendo-react-buttons'
import { observer } from 'mobx-react'
import UsersList from '../UsersList'
import NewUserDialog from '../NewUserDialog'
import Props from '../../types/Home'

function Home({ nodes, onCreate, dialogOpen, onToggleDialog, loading }: Props) {
  return (
    <>
      <Button
        onClick={onToggleDialog}
        themeColor="primary"
        icon="add"
        style={{ marginBottom: 8 }}
      >
        New user
      </Button>

      {nodes.length > 0 && <UsersList rows={nodes} />}

      {dialogOpen && (
        <NewUserDialog
          onClose={onToggleDialog}
          onSubmit={onCreate}
          loading={loading}
        />
      )}
    </>
  )
}

export default observer(Home)
