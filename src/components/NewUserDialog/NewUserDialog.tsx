import React from 'react'
import { Dialog } from '@progress/kendo-react-dialogs'
import NewUserDialogForm from '../NewUserDialogForm'
import Props from '../../types/NewUserDialog'

function NewUserDialog({ onClose, onSubmit, loading }: Props) {
  return (
    <Dialog title="Create new user" onClose={onClose}>
      <NewUserDialogForm
        onSubmit={onSubmit}
        loading={loading}
        submitButtonLabel="Create"
        label="Please fill in the following information"
      />
    </Dialog>
  )
}

export default NewUserDialog
