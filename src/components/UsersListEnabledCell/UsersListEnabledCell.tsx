import React from 'react'
import { useTableKeyboardNavigation } from '@progress/kendo-react-data-tools'
import { GRID_COL_INDEX_ATTRIBUTE } from '@progress/kendo-react-grid'
import { GridCellProps } from '@progress/kendo-react-grid/dist/npm/interfaces/GridCellProps'

function UsersListEnabledCell(props: GridCellProps) {
  const field = props.field || ''
  const value = props.dataItem[field]
  const navigationAttributes = useTableKeyboardNavigation(props.id)
  return (
    <td
      style={{
        color: value ? 'green' : 'red'
      }}
      colSpan={props.colSpan}
      role="gridcell"
      aria-colindex={props.ariaColumnIndex}
      aria-selected={props.isSelected}
      {...{
        [GRID_COL_INDEX_ATTRIBUTE]: props.columnIndex
      }}
      {...navigationAttributes}
    >
      {value ? 'Yes' : 'No'}
    </td>
  )
}

export default UsersListEnabledCell
