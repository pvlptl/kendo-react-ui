import React from 'react'
import { Card, CardBody } from '@progress/kendo-react-layout'
import NewUserDialogForm from '../NewUserDialogForm'

function UserDetails({ user, loading, onSubmit }: any) {
  const { firstName, lastName, userName, enabled } = user

  return (
    <Card
      style={{
        width: 260,
        boxShadow: '0 0 4px 0 rgba(0, 0, 0, .1)',
        marginTop: '15px'
      }}
    >
      <CardBody>
        <NewUserDialogForm
          onSubmit={onSubmit}
          loading={loading}
          label="User Details"
          submitButtonLabel="Update"
          defaultValues={{
            userName,
            firstName,
            lastName,
            enabled
          }}
        />
      </CardBody>
    </Card>
  )
}

export default UserDetails
