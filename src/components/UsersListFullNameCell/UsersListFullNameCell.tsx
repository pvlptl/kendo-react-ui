import React from 'react'
import { useTableKeyboardNavigation } from '@progress/kendo-react-data-tools'
import { GRID_COL_INDEX_ATTRIBUTE } from '@progress/kendo-react-grid'
import { GridCellProps } from '@progress/kendo-react-grid/dist/npm/interfaces/GridCellProps'

function UsersListFullNameCell(props: GridCellProps) {
  const { firstName, lastName } = props.dataItem
  const navigationAttributes = useTableKeyboardNavigation(props.id)
  return (
    <td
      colSpan={props.colSpan}
      role="gridcell"
      aria-colindex={props.ariaColumnIndex}
      aria-selected={props.isSelected}
      {...{
        [GRID_COL_INDEX_ATTRIBUTE]: props.columnIndex
      }}
      {...navigationAttributes}
    >
      {`${firstName} ${lastName}`}
    </td>
  )
}

export default UsersListFullNameCell
