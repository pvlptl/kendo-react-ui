import React, { useState, useMemo } from 'react'
import { useNavigate } from 'react-router-dom'
import {
  Grid,
  GridColumn,
  GridSortChangeEvent,
  GridFilterChangeEvent
} from '@progress/kendo-react-grid'
import UsersListEnabledCell from '../UsersListEnabledCell'
import UsersListLastLoginCell from './UsersListLastLoginCell'
import { orderBy, SortDescriptor } from '@progress/kendo-data-query'
import { filterBy, CompositeFilterDescriptor } from '@progress/kendo-data-query'
import './styles.css'
import { GridRowClickEvent } from '@progress/kendo-react-grid/dist/npm/interfaces/events'
import UsersListFullNameCell from '../UsersListFullNameCell'
import Props from '../../types/UsersList'

const initialSort: Array<SortDescriptor> = [{ field: 'userName', dir: 'asc' }]

const initialFilter: CompositeFilterDescriptor = {
  logic: 'and',
  filters: []
}

function UsersList({ rows }: Props) {
  const navigate = useNavigate()
  const [sort, setSort] = useState(initialSort)
  const [filter, setFilter] = useState(initialFilter)

  const handleChangeSort = (e: GridSortChangeEvent) => setSort(e.sort)
  const handleChangeFilter = (e: GridFilterChangeEvent) => setFilter(e.filter)

  const data = useMemo(
    () => filterBy(orderBy(rows, sort), filter),
    [rows, sort, filter]
  )

  return (
    <Grid
      data={data}
      sortable
      sort={sort}
      filterable
      filter={filter}
      onFilterChange={handleChangeFilter}
      onSortChange={handleChangeSort}
      onRowClick={(e: GridRowClickEvent) => navigate(`/user/${e.dataItem.id}`)}
    >
      <GridColumn field="userName" />
      <GridColumn field="fullName" cell={UsersListFullNameCell} />
      <GridColumn field="lastLogin" cell={UsersListLastLoginCell} />
      <GridColumn field="enabled" cell={UsersListEnabledCell} />
    </Grid>
  )
}

export default UsersList
