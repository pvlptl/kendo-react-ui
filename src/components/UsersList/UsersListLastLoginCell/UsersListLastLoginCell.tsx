import React from 'react'
import { useTableKeyboardNavigation } from '@progress/kendo-react-data-tools'
import { GRID_COL_INDEX_ATTRIBUTE } from '@progress/kendo-react-grid'
import moment from 'moment'
import { GridCellProps } from '@progress/kendo-react-grid/dist/npm/interfaces/GridCellProps'

function UsersListLastLoginCell(props: GridCellProps) {
  const field = props.field || ''
  const value = props.dataItem[field]
  const navigationAttributes = useTableKeyboardNavigation(props.id)
  return (
    <td
      colSpan={props.colSpan}
      role="gridcell"
      aria-colindex={props.ariaColumnIndex}
      aria-selected={props.isSelected}
      {...{
        [GRID_COL_INDEX_ATTRIBUTE]: props.columnIndex
      }}
      {...navigationAttributes}
    >
      {moment(value).format('DD-MM-YYYY hh:mm:ss')}
    </td>
  )
}

export default UsersListLastLoginCell
