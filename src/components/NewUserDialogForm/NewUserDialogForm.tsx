import React, { useState } from 'react'
import {
  Form,
  Field,
  FormElement,
  FieldRenderProps,
  FormRenderProps
} from '@progress/kendo-react-form'
import { Error } from '@progress/kendo-react-labels'
import { Button } from '@progress/kendo-react-buttons'
import { Input, Checkbox } from '@progress/kendo-react-inputs'
import Props from '../../types/NewUserDialogForm'
import FormValues from '../../types/FormValues'

const CustomInput = (fieldRenderProps: FieldRenderProps) => {
  const { validationMessage, visited, ...others } = fieldRenderProps
  return (
    <div>
      <Input {...others} />
      {visited && validationMessage && <Error>{validationMessage}</Error>}
    </div>
  )
}

function NewUserDialogForm({
  onSubmit,
  loading,
  label,
  submitButtonLabel,
  defaultValues
}: Props) {
  const [values, setValues] = useState<FormValues>(
    defaultValues || {
      userName: '',
      firstName: '',
      lastName: '',
      enabled: false
    }
  )

  const handleChange = (e: any) =>
    setValues((prev: any) => ({
      ...prev,
      [e.target.name]: e.target.value
    }))

  const handleSubmit = () => onSubmit(values)

  return (
    <Form
      initialValues={values}
      onSubmitClick={handleSubmit}
      render={(formRenderProps: FormRenderProps) => (
        <FormElement>
          <fieldset className={'k-form-fieldset'}>
            <legend className={'k-form-legend'}>{label}</legend>
            {formRenderProps.visited &&
              formRenderProps.errors &&
              formRenderProps.errors.VALIDATION_SUMMARY && (
                <div className={'k-messagebox k-messagebox-error'}>
                  {formRenderProps.errors.VALIDATION_SUMMARY}
                </div>
              )}

            <Field
              onChange={handleChange}
              component={CustomInput}
              name="userName"
              label="Username"
              pattern="[A-Za-z]+"
              minLength={2}
              maxLength={15}
              required
              value={values.userName}
              validator={(value: string) => {
                if (value?.length === 0) return 'Required field'
                else if (/\d/.test(value)) return 'Only letters allowed'
              }}
            />

            <div style={{ display: 'flex', gap: 8 }}>
              <Field
                onChange={handleChange}
                component={CustomInput}
                name="firstName"
                value={values.firstName}
                label="Firstname"
                pattern="[A-Za-z]+"
                minLength={2}
                maxLength={25}
                required
                validator={(value: string) => {
                  if (value?.length === 0) return 'Required field'
                  else if (/\d/.test(value)) return 'Only letters allowed'
                }}
              />

              <Field
                onChange={handleChange}
                component={CustomInput}
                name="lastName"
                value={values.lastName}
                label="Lastname"
                pattern="[A-Za-z]+"
                minLength={2}
                maxLength={25}
                required
                validator={(value: string) => {
                  if (value?.length === 0) return 'Required field'
                  else if (/\d/.test(value)) return 'Only letters allowed'
                }}
              />
            </div>

            <Checkbox
              onChange={handleChange}
              name="enabled"
              style={{ marginTop: 8 }}
              defaultChecked={values.enabled}
              label="Enabled"
            />
          </fieldset>
          <Button
            disabled={loading}
            style={{ marginTop: 8, float: 'right' }}
            type="submit"
            themeColor="primary"
            icon="save"
          >
            {loading ? 'Loading...' : submitButtonLabel}
          </Button>
        </FormElement>
      )}
    />
  )
}

export default NewUserDialogForm
