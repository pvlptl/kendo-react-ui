import { makeAutoObservable } from 'mobx'
import { faker } from '@faker-js/faker'
import User from '../types/User'
import AddOneArgs from '../types/AddOneArgs'
import UpdateOneByIdArgs from '../types/UpdateOneByIdArgs'

export class ApplicationStore {
  nodes: User[] = []

  constructor() {
    makeAutoObservable(this)
  }

  findOneById = (id: string) => {
    return this.nodes.find((i: User) => i.id === id)
  }

  addOne = async ({ values, onSuccess }: AddOneArgs) => {
    return new Promise(() => {
      setTimeout(() => {
        this.nodes = [
          {
            id: faker.datatype.uuid(),
            userName: values.userName,
            firstName: values.firstName,
            lastName: values.lastName,
            lastLogin: faker.date.past(),
            enabled: values.enabled
          },
          ...this.nodes
        ]

        if (onSuccess) onSuccess()
      }, 1000)
    })
  }

  updateOneById = async ({ id, data, onSuccess }: UpdateOneByIdArgs) => {
    return new Promise(() => {
      setTimeout(() => {
        const newNodes = [...this.nodes]

        const idx = newNodes.findIndex(i => i.id === id)

        newNodes[idx] = {
          ...newNodes[idx],
          ...data
        }

        this.nodes = newNodes

        if (onSuccess) onSuccess()
      }, 1000)
    })
  }
}
