import React from 'react'
import ReactDOM from 'react-dom'
import { AppContext, stores } from './AppContext'
import { BrowserRouter } from 'react-router-dom'
import App from './App'

ReactDOM.render(
  <React.StrictMode>
    <AppContext.Provider value={stores}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </AppContext.Provider>
  </React.StrictMode>,
  document.getElementById('root')
)
