import React from 'react'
import '@progress/kendo-theme-default/dist/all.css'
import { Routes, Route } from 'react-router-dom'
import HomeConnected from './containers/HomeConnected'
import UserDetailConnected from './containers/UserDetailConnected'

function App() {
  return (
    <Routes>
      <Route path="/" element={<HomeConnected />} />
      <Route path="/user/:id" element={<UserDetailConnected />} />
    </Routes>
  )
}

export default App
