import FormValues from './FormValues'

interface UpdateOneByIdArgs {
  id: string
  data: FormValues
  onSuccess: () => void
}

export default UpdateOneByIdArgs
