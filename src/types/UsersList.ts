import User from './User'

interface UsersList {
  rows: User[]
}

export default UsersList
