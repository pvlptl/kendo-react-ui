import User from './User'
import FormValues from './FormValues'

interface Home {
  nodes: User[]
  onCreate: (values: FormValues) => void
  onToggleDialog: () => void
  dialogOpen: boolean
  loading: boolean
}

export default Home
