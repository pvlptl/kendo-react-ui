interface User {
  id: string
  userName: string
  firstName: string
  lastName: string
  lastLogin: Date
  enabled: boolean
}

export default User
