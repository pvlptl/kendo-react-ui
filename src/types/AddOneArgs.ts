import FormValues from './FormValues'

interface AddOneArgs {
  values: FormValues
  onSuccess: () => void
}

export default AddOneArgs
