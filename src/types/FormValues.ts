interface FormValues {
  userName: string
  firstName: string
  lastName: string
  enabled: boolean
}

export default FormValues
