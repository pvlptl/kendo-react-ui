import FormValues from './FormValues'

interface NewUserDialog {
  onSubmit: (values: FormValues) => void
  loading: boolean
  label: string
  submitButtonLabel: string
  defaultValues?: FormValues
}

export default NewUserDialog
