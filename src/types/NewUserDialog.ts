import FormValues from './FormValues'

interface NewUserDialog {
  onSubmit: (values: FormValues) => void
  onClose: () => void
  loading: boolean
}

export default NewUserDialog
