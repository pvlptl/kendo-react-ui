import React, { useContext, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import { AppContext } from '../../AppContext'
import UserDetails from '../../components/UserDetails'

function UserDetailConnected() {
  const [loading, setLoading] = useState<boolean>(false)

  const { id } = useParams()
  const navigate = useNavigate()
  const {
    applicationStore: { findOneById, updateOneById }
  } = useContext(AppContext)

  const user = findOneById(id as string)

  const handleSubmit = async (data: any) => {
    setLoading(true)
    await updateOneById({
      id: id as string,
      data,
      onSuccess: () => navigate('/')
    })
    setLoading(true)
  }

  return <UserDetails user={user} onSubmit={handleSubmit} loading={loading} />
}

export default UserDetailConnected
