import React, { useContext, useState } from 'react'
import { AppContext } from '../../AppContext'
import Home from '../../components/Home'
import FormValues from '../../types/FormValues'

function HomeConnected() {
  const [loading, setLoading] = useState<boolean>(false)
  const [open, setOpen] = useState<boolean>(false)

  const handleToggleModal = () => setOpen(prev => !prev)

  const { applicationStore } = useContext(AppContext)
  const { nodes } = applicationStore

  const handleCreate = async (values: FormValues) => {
    setLoading(true)
    await applicationStore.addOne({
      values,
      onSuccess: handleToggleModal
    })
    setLoading(true)
  }

  return (
    <Home
      nodes={nodes}
      dialogOpen={open}
      loading={loading}
      onCreate={handleCreate}
      onToggleDialog={handleToggleModal}
    />
  )
}

export default HomeConnected
